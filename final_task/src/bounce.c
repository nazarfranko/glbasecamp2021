#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include "ili9341.h"

#define BUFFER_LENGTH 10
static char receive[BUFFER_LENGTH]; 

void lcd_fill_rectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);
void lcd_fill_circle(uint16_t x, uint16_t y, uint16_t r, uint16_t color);
void write_dev();
void read_dev();
void check_pos();

struct Ball
{
	uint16_t x;
	uint16_t y;
	uint16_t radius;
	uint16_t color;
} ball;

static uint16_t frame_buff[LCD_WIDTH * LCD_HEIGHT];
static bool keep_running = true;

int main(void)
{
	lcd_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, COLOR_BLUE);
	// appear new ball in top center
	ball.x = (LCD_WIDTH - 1) / 2;
	ball.y = 25;
	ball.radius = 25;
	ball.color = COLOR_RED;
	// draw ball
	lcd_fill_circle(ball.x, ball.y, ball.radius, ball.color);
	printf("Hello there!\n");
	write_dev();
	
	while(keep_running)
	{
		// read keys
		read_dev();
		// ball falls on 10 points
		ball.y += 10;
		// check bounds
		check_pos();
		// update screen
		lcd_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, COLOR_BLUE);
		lcd_fill_circle(ball.x, ball.y, ball.radius, ball.color);
		write_dev();
	}
	
	lcd_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, COLOR_BLUE);
	write_dev();
	printf("Bye!\n");
	
	return 0;
}

void lcd_fill_rectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
	uint16_t i;
	uint16_t j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}

	if ((x + w - 1) > LCD_WIDTH) {
		w = LCD_WIDTH - x;
	}

	if ((y + h - 1) > LCD_HEIGHT) {
		h = LCD_HEIGHT - y;
	}

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			frame_buff[(x + LCD_WIDTH * y) + (i + LCD_WIDTH * j)] = (color >> 8) | (color << 8);
		}
	}
}

void lcd_fill_circle(uint16_t x, uint16_t y, uint16_t r, uint16_t color)
{
    uint16_t i, j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}
    
    
	if ((x + r - 1) > LCD_WIDTH) {
		r = LCD_WIDTH - x;
	}

	if ((y + r - 1) > LCD_HEIGHT) {
		r = LCD_HEIGHT - y;
	}
    
    for (i = x - r; i < x + r; i++)
        for (j = y - r; j < y + r; j++) {
            if ( (i-x)*(i-x) + (j-y)*(j-y) <= r * r) {
				frame_buff[(i + LCD_WIDTH * j)] = (color >> 8) | (color << 8);
        }    
    }
}

void write_dev()
{
	char path[] = "/dev/mychardev-1";
	int fd = open(path, O_WRONLY);
 	if (fd < 0) {
		printf("Device access error, fd = %d\n", fd);
		return;
	}
	write(fd, &frame_buff, sizeof(frame_buff) * sizeof(uint16_t));
	close(fd);
}

void read_dev()
{
	int ret;
	char msg[] = "XX";
	
	char path[] = "/dev/mychardev-1";
	int fd = open(path, O_RDONLY);
	if (fd < 0) {
		printf("Device access error, fd = %d\n", fd);
		return;
	}
	ret = read(fd, receive, BUFFER_LENGTH);
	if (ret < 0){
		perror("Failed to read the message from the device.");
		return;
	}
	
	if (!strcmp(receive, "EX"))
		keep_running = false;
	if (!strcmp(receive, "X1"))
	{
		ball.x -= 10;
		printf("LEFT!\n");
	}
	if (!strcmp(receive, "X2"))
	{
		ball.y = ball.radius;
		printf("UP!\n");
	}
	if (!strcmp(receive, "X3"))
	{
		ball.x += 10;
		printf("RIGHT!\n");
	}	
	
	close(fd);
}

void check_pos()
{
	if (ball.y > LCD_HEIGHT - 1 - ball.radius)
	{
		ball.y = LCD_HEIGHT - 1 - ball.radius;
	}
	if (ball.x > LCD_WIDTH - 1 - ball.radius)
	{
		ball.x = LCD_WIDTH - 1 - ball.radius;
	}
	if (ball.x - ball.radius < 0)
	{
		ball.x = ball.radius;
	}
}
