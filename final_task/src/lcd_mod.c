#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include "ili9341.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nazar Franko <nazarfrenk77@gmail.com>");
MODULE_DESCRIPTION("Fianl task by Nazar Franko");
MODULE_VERSION("0.1");

#define DATA_SIZE	90

static u16 frame_buffer[LCD_WIDTH * LCD_HEIGHT];
static struct spi_device *lcd_spi_device;

static int irqX1, irqX2, irqX3;
static u8 x1 = 1, x2 = 1, x3 = 1;
static char *state = "XX";
static size_t proc_msg_length;
static size_t proc_msg_read_pos;
 
static ssize_t mychardev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);
static ssize_t mychardev_read(struct file *file, char __user *buf, size_t count, loff_t *offset);

// initialize file_operations
static const struct file_operations mychardev_fops = {
    .owner      = THIS_MODULE,
    .write      = mychardev_write,
    .read       = mychardev_read
};

// device data holder, this structure may be extended to hold additional data
struct mychar_device_data {
    struct cdev cdev;
} mychardev_data;

// global storage for device Major number
static int dev_major = 0;

// sysfs class structure
static struct class *mychardev_class = NULL;

static int mychardev_uevent(struct device *dev, struct kobj_uevent_env *env)
{
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return 0;
}

static irqreturn_t lcd_key_isr(int irq, void *data) 
{     
    x1 = gpio_get_value(GPIO_X1); 
    printk(KERN_INFO "LCD_KEY: key1=%d\n", x1); 
    x2 = gpio_get_value(GPIO_X2); 
    printk(KERN_INFO "LCD_KEY: key2=%d\n", x2); 
    x3 = gpio_get_value(GPIO_X3); 
    printk(KERN_INFO "LCD_KEY: key3=%d\n", x3);
    
    return IRQ_HANDLED; 
} 

static void lcd_reset(void)
{
	gpio_set_value(LCD_PIN_RESET, 0);
	mdelay(5);
	gpio_set_value(LCD_PIN_RESET, 1);
}

static void lcd_write_command(u8 cmd)
{
	gpio_set_value(LCD_PIN_DC, 0);
	spi_write(lcd_spi_device, &cmd, sizeof(cmd));
}

static void lcd_write_data(u8 *buff, size_t buff_size)
{
	size_t i = 0;
	
	gpio_set_value(LCD_PIN_DC, 1);
	while (buff_size > DATA_SIZE) {
		spi_write(lcd_spi_device, buff + i, DATA_SIZE);
		i += DATA_SIZE;
		buff_size -= DATA_SIZE;
	}
	spi_write(lcd_spi_device, buff + i, buff_size);
}

static void lcd_set_address_window(u16 x0, u16 y0, u16 x1, u16 y1)
{

	lcd_write_command(LCD_CASET);
	{
		uint8_t data[] = { (x0 >> 8) & 0xFF, x0 & 0xFF,
				 (x1 >> 8) & 0xFF, x1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RASET);
	{
		uint8_t data[] = { (y0 >> 8) & 0xFF, y0 & 0xFF,
				(y1 >> 8) & 0xFF, y1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RAMWR);
}

inline void lcd_update_screen(void)
{
	lcd_write_data((u8*)frame_buffer, sizeof(u16) * LCD_WIDTH * LCD_HEIGHT);
}

void lcd_draw_pixel(u16 x, u16 y, u16 color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}
        
	frame_buffer[x + LCD_WIDTH * y] = (color >> 8) | (color << 8);
	lcd_update_screen();
}

void lcd_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color)
{
	u16 i;
	u16 j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}

	if ((x + w - 1) > LCD_WIDTH) {
		w = LCD_WIDTH - x;
	}

	if ((y + h - 1) > LCD_HEIGHT) {
		h = LCD_HEIGHT - y;
	}

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			frame_buffer[(x + LCD_WIDTH * y) + (i + LCD_WIDTH * j)] = (color >> 8) | (color << 8);
		}
	}
	//lcd_update_screen();
}

void lcd_fill_circle(u16 x, u16 y, u16 r, u16 color)
{
    u16 i, j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}
    
    
	if ((x + r - 1) > LCD_WIDTH) {
		r = LCD_WIDTH - x;
	}

	if ((y + r - 1) > LCD_HEIGHT) {
		r = LCD_HEIGHT - y;
	}
    
    for (i = x - r; i < x + r; i++)
        for (j = y - r; j < y + r; j++) {
            if ( (i-x)*(i-x) + (j-y)*(j-y) <= r * r) {
            frame_buffer[(i + LCD_WIDTH * j)] = (color >> 8) | (color << 8);
        }    
    }
    
    //lcd_update_screen();
}

void lcd_fill_screen(u16 color)
{
	lcd_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, color);
}

void lcd_init_ili9341(void)
{
	// SOFTWARE RESET
	lcd_write_command(0x01);
	mdelay(1000);

	// POWER CONTROL A
	lcd_write_command(0xCB);
	{
		u8 data[] = { 0x39, 0x2C, 0x00, 0x34, 0x02 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL B
	lcd_write_command(0xCF);
	{
		u8 data[] = { 0x00, 0xC1, 0x30 };
		lcd_write_data(data, sizeof(data));
	}

	// DRIVER TIMING CONTROL A
	lcd_write_command(0xE8);
	{
		u8 data[] = { 0x85, 0x00, 0x78 };
		lcd_write_data(data, sizeof(data));
	}

	// DRIVER TIMING CONTROL B
	lcd_write_command(0xEA);
	{
		u8 data[] = { 0x00, 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER ON SEQUENCE CONTROL
	lcd_write_command(0xED);
	{
		u8 data[] = { 0x64, 0x03, 0x12, 0x81 };
		lcd_write_data(data, sizeof(data));
	}

	// PUMP RATIO CONTROL
	lcd_write_command(0xF7);
	{
		u8 data[] = { 0x20 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL,VRH[5:0]
	lcd_write_command(0xC0);
	{
		u8 data[] = { 0x23 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL,SAP[2:0];BT[3:0]
	lcd_write_command(0xC1);
	{
		u8 data[] = { 0x10 };
		lcd_write_data(data, sizeof(data));
	}

	// VCM CONTROL
	lcd_write_command(0xC5);
	{
		u8 data[] = { 0x3E, 0x28 };
		lcd_write_data(data, sizeof(data));
	}

	// VCM CONTROL 2
	lcd_write_command(0xC7);
	{
		u8 data[] = { 0x86 };
		lcd_write_data(data, sizeof(data));
	}

	// PIXEL FORMAT
	lcd_write_command(0x3A);
	{
		u8 data[] = { 0x55 };
		lcd_write_data(data, sizeof(data));
	}
	
	// FRAME RATIO CONTROL, STANDARD RGB COLOR
	lcd_write_command(0xB1);
	{
		u8 data[] = { 0x00, 0x18 };
		lcd_write_data(data, sizeof(data));
	}
	
	// DISPLAY FUNCTION CONTROL
	lcd_write_command(0xB6);
	{
		u8 data[] = { 0x08, 0x82, 0x27 };
		lcd_write_data(data, sizeof(data));
	}

	// 3GAMMA FUNCTION DISABLE
	lcd_write_command(0xF2);
	{
		u8 data[] = { 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// GAMMA CURVE SELECTED
	lcd_write_command(0x26);
	{
		u8 data[] = { 0x01 };
		lcd_write_data(data, sizeof(data));
	}
	
	// POSITIVE GAMMA CORRECTION
	lcd_write_command(0xE0);
	{
		u8 data[] = { 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08, 0x4E, 0xF1,
                           0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00 };
		lcd_write_data(data, sizeof(data));
	}
	
	// NEGATIVE GAMMA CORRECTION
	lcd_write_command(0xE1);
	{
		u8 data[] = { 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07, 0x31, 0xC1,
                           0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F };
		lcd_write_data(data, sizeof(data));
	}

	// EXIT SLEEP
	lcd_write_command(0x11);
	mdelay(120);
    
	// TURN ON DISPLAY
	lcd_write_command(0x29);

	// MEMORY ACCESS CONTROL
	lcd_write_command(0x36);
	{
		u8 data[] = { 0x98 };
		lcd_write_data(data, sizeof(data));
	}

	// INVERSION
//	lcd_write_command(0x21);
}

static void __exit mod_exit(void)
{
    free_irq(irqX1, NULL);
    free_irq(irqX2, NULL);
    free_irq(irqX3, NULL);
	
    gpio_free(LCD_PIN_DC);
    
    gpio_free(GPIO_X1);
    gpio_free(GPIO_X2);
    gpio_free(GPIO_X3);
//	gpio_free(LCD_PIN_RESET);

	if (lcd_spi_device) {
		spi_unregister_device(lcd_spi_device);
	}
    
    device_destroy(mychardev_class, MKDEV(dev_major, 1));
    class_unregister(mychardev_class);
    class_destroy(mychardev_class);

    unregister_chrdev_region(MKDEV(dev_major, 0), MINORMASK);
    
	pr_info("LCD: spi device unregistered\n");
	pr_info("LCD: module exited\n");
}

static int __init mod_init(void)
{
	int ret;
	struct spi_master *master;
    
    dev_t dev;

	struct spi_board_info lcd_info = {
		.modalias = "LCD",
		.max_speed_hz = 25e6,
		.bus_num = 0,
		.chip_select = 0,
		.mode = SPI_MODE_0,
	};

	master = spi_busnum_to_master(lcd_info.bus_num);
	if (!master) {
		printk("MASTER not found.\n");
		ret = -ENODEV;
		goto out;
	}

	lcd_spi_device = spi_new_device(master, &lcd_info);
	if (!lcd_spi_device) {
		printk("FAILED to create slave.\n");
		ret = -ENODEV;
		goto out;
	}

	lcd_spi_device->bits_per_word = 8,

	ret = spi_setup(lcd_spi_device);
	if (ret) {
		printk("FAILED to setup slave.\n");
		spi_unregister_device(lcd_spi_device);
		ret = -ENODEV;
		goto out;
	}

	pr_info("LCD: spi device setup completed\n");

	gpio_request(LCD_PIN_RESET, "LCD_PIN_RESET");
	gpio_direction_output(LCD_PIN_RESET, 0);
	gpio_request(LCD_PIN_DC, "LCD_PIN_DC");
	gpio_direction_output(LCD_PIN_DC, 0);
        
    ret = gpio_request(GPIO_X1, "LCD_KEY_X1");
    if (ret) {
        printk(KERN_ERR "LCD_KEY: failed to request GPIO%d: %d\n", GPIO_X1, ret);
        gpio_free(LCD_PIN_DC);
        return -1;
    }
    
    ret = gpio_request(GPIO_X2, "LCD_KEY_X2");
    if (ret) {
        printk(KERN_ERR "LCD_KEY: failed to request GPIO%d: %d\n", GPIO_X1, ret);
        gpio_free(LCD_PIN_DC);
        gpio_free(GPIO_X1);
        return -1;
    }
    
    ret = gpio_request(GPIO_X3, "LCD_KEY_X3");
    if (ret) {
        printk(KERN_ERR "LCD_KEY: failed to request GPIO%d: %d\n", GPIO_X1, ret);
        gpio_free(LCD_PIN_DC);
        gpio_free(GPIO_X1);
        gpio_free(GPIO_X2);
        return -1;
    }
    
    gpio_direction_input(GPIO_X1);
    gpio_direction_input(GPIO_X2);
    gpio_direction_input(GPIO_X3);
    
    ret = gpio_to_irq(GPIO_X1);
    if (ret < 0) { 
        printk(KERN_ERR "LCD_KEY: failed to request IRQ: %d\n", ret); 
        gpio_free(LCD_PIN_DC);
        gpio_free(GPIO_X1);
        gpio_free(GPIO_X2);
        gpio_free(GPIO_X3);
        return -1; 
    }
    
    irqX1 = ret;
    printk(KERN_INFO "LCD_KEY: requested IRQ#%d.\n", irqX1);
    ret = request_irq(irqX1, lcd_key_isr, IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING, "lcd_key_irq", NULL);
    if (ret) {
        printk(KERN_ERR "LCD_KEY: failed to request IRQ\n"); 
        gpio_free(LCD_PIN_DC);
        gpio_free(GPIO_X1);
        gpio_free(GPIO_X2);
        gpio_free(GPIO_X3);
        return -1;
    }
    
    ret = gpio_to_irq(GPIO_X2);
    if (ret < 0) { 
        printk(KERN_ERR "LCD_KEY: failed to request IRQ: %d\n", ret); 
        gpio_free(LCD_PIN_DC);
        gpio_free(GPIO_X1);
        gpio_free(GPIO_X2);
        gpio_free(GPIO_X3);
        return -1; 
    }
    
    irqX2 = ret;
    printk(KERN_INFO "LCD_KEY: requested IRQ#%d.\n", irqX2);
    ret = request_irq(irqX2, lcd_key_isr, IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING, "lcd_key_irq", NULL);
    if (ret) {
        printk(KERN_ERR "LCD_KEY: failed to request IRQ\n"); 
        gpio_free(LCD_PIN_DC);
        gpio_free(GPIO_X1);
        gpio_free(GPIO_X2);
        gpio_free(GPIO_X3);
        return -1;
    }
    
    ret = gpio_to_irq(GPIO_X3);
    if (ret < 0) { 
        printk(KERN_ERR "LCD_KEY: failed to request IRQ: %d\n", ret); 
        gpio_free(LCD_PIN_DC);
        gpio_free(GPIO_X1);
        gpio_free(GPIO_X2);
        gpio_free(GPIO_X3);
        return -1; 
    }
    
    irqX3 = ret;
    printk(KERN_INFO "LCD_KEY: requested IRQ#%d.\n", irqX3);
    ret = request_irq(irqX3, lcd_key_isr, IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING, "lcd_key_irq", NULL);
    if (ret) {
        printk(KERN_ERR "LCD_KEY: failed to request IRQ\n"); 
        gpio_free(LCD_PIN_DC);
        gpio_free(GPIO_X1);
        gpio_free(GPIO_X2);
        gpio_free(GPIO_X3);
        return -1;
    }
    
    alloc_chrdev_region(&dev, 0, 1, "mychardev");
    
    dev_major = MAJOR(dev);

    mychardev_class = class_create(THIS_MODULE, "mychardev");
    mychardev_class->dev_uevent = mychardev_uevent;

    cdev_init(&mychardev_data.cdev, &mychardev_fops);
    mychardev_data.cdev.owner = THIS_MODULE;

    cdev_add(&mychardev_data.cdev, MKDEV(dev_major, 1), 1);

    device_create(mychardev_class, NULL, MKDEV(dev_major, 1), NULL, "mychardev-%d", 1);

	lcd_reset();

	lcd_init_ili9341();

	memset(frame_buffer, COLOR_BLACK, sizeof(frame_buffer));
	lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);

	lcd_update_screen();

 	pr_info("LCD: module loaded\n");

	return 0;

out:
	return ret;
}

static ssize_t mychardev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
    size_t maxdatalen = 10, ncopied;
    
    if (maxdatalen < count)
    {
        maxdatalen = LCD_WIDTH * LCD_HEIGHT * sizeof(u16); 

        ncopied = copy_from_user(frame_buffer, buf, maxdatalen);
        
        lcd_update_screen();
    }
    else
    {
        ncopied = copy_from_user(state, buf, maxdatalen);
        printk(KERN_NOTICE "mychardev_write: From user %s, %d\n", buf, sizeof(buf));

    }
    
    proc_msg_length = maxdatalen - ncopied;
    proc_msg_read_pos = 0;
        
    if (ncopied == 0) {
        printk("mychardev_write: Copied %zd bytes from the user\n", maxdatalen);
    } else {
        printk("mychardev_write: Could't copy %zd bytes from the user\n", ncopied);
    }
    
    return count;
}

static ssize_t mychardev_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
    size_t left;
    
    if (count > (proc_msg_length - proc_msg_read_pos))
        count = (proc_msg_length - proc_msg_read_pos);
    
    if (x1 == 0 && x3 == 0)
    {
        state = "EX";
    }
    else
    {
        if (x1 == 0)
            state = "X1";
        else if (x2 == 0)
            state = "X2";
        else if (x3 == 0)
            state = "X3";
        else
            state = "XX";
    }
    
    printk(KERN_NOTICE "mychardev_read: State =%s, x1=%d, x2=%d, x3=%d\n", state, x1, x2, x3);
    
    left = copy_to_user(buf, &state[proc_msg_read_pos], count);
    
    proc_msg_read_pos += count - left;
    printk(KERN_NOTICE "mychardev_read: To user buf=%s, sizeof_buf=%d, state=%s, sizeof_state=%d\n", buf, sizeof(buf), state, sizeof(state));
    
    if (left)
        printk(KERN_ERR "mychardev_read: Failed to read %u from %u chars\n", left, count);
    else
        printk(KERN_NOTICE "mychardev_read: Read %u chars\n", count);
    
    return count - left;

}

module_init(mod_init);
module_exit(mod_exit);
