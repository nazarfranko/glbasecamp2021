# Author

- Nazar Franko
- Kharkiv

# Title

Bounce

# Description

1. Purpose of development.
2. Raspberry Pi Zero, 3.2" 320x240 TFT LCD.

## References

1. [Video of test program](https://drive.google.com/file/d/1-znhb0U5tSbrYmfdfO0CM7g96fFqLNsJ/view?usp=sharing).
2. [Photo of screen at start](https://drive.google.com/file/d/101mTgLm9IKBMvEn9bme_KDWdJMjcj813/view?usp=sharing).
3. [Photo of screen after X2 pressed](https://drive.google.com/file/d/10AIOimEj5uMZZ0K15SGM-dNtT31QBQ8t/view?usp=sharing).
4. [Photo of screen after X1 pressed several times](https://drive.google.com/file/d/102Ew1ALate16h29GXa2F60duoyq2QpvQ/view?usp=sharing).
5. [Photo of screen exit](https://drive.google.com/file/d/100ZcSdCgXTeZtuA-SKrTRL5DAUVb7Me5/view?usp=sharing).
